//
//  ViewController.swift
//  Domaci1
//
//  Created by Luka on 23/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    weak var delegate: ViewControllerDelegate?
    @IBOutlet weak var myTable: UITableView!
    var labelArray = ["me","you","us","zombie","lelel"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib.init(nibName: "CustomCell", bundle: nil)
        myTable.delegate = self
        myTable.dataSource = self
        myTable.register(nib, forCellReuseIdentifier: "CustomCell")
    }
    
    @IBAction func transferButtonTapped(_ sender: UIButton) {
        delegate?.go2Tapped()
    }
    
    @IBAction func transferButtonTapped2(_ sender: UIButton) {
        delegate?.go3Tapped()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomCell
        cell.cellLbl.text = labelArray[indexPath.row]
        return cell
    }
}

