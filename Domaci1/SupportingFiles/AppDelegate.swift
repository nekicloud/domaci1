//
//  AppDelegate.swift
//  Domaci1
//
//  Created by Luka on 23/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let appCoord = AppCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        appCoord.start()
        return true
    }

}

