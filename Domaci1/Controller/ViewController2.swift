//
//  ViewController2.swift
//  Domaci1
//
//  Created by Luka on 23/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//


import UIKit

class ViewController2: UIViewController {
    
    weak var delegate: ViewControllerDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    var labelArray = ["me","you","us","zombie","lelel"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib.init(nibName: "CollectionCell", bundle: nil)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(nib, forCellWithReuseIdentifier: "CollectionCell")
    }
    
    @IBAction func transferButtonTapped(_ sender: UIButton) {
        delegate?.go1Tapped()
    }
    
    @IBAction func transferButtonTapped2(_ sender: UIButton) {
        delegate?.go3Tapped()
    }
}

extension ViewController2 : UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return labelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        cell.cellLbl.text = labelArray[indexPath.row]
        return cell
    }
}
