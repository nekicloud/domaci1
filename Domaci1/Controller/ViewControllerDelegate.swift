//
//  ViewControllerDelegate.swift
//  Domaci1
//
//  Created by Luka on 23/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol ViewControllerDelegate: class {
    func go1Tapped()
    func go2Tapped()
    func go3Tapped()
}
