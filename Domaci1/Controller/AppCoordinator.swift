//
//  AppCoordinator.swift
//  Domaci1
//
//  Created by Luka on 23/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol ViewControllerDelegate: class {
    func go1Tapped()
    func go2Tapped()
    func go3Tapped()
}


class AppCoordinator {
    
    let window: UIWindow
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
    }
    
    func start() {
        let vc = ViewController()
        vc.delegate = self
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
}

extension AppCoordinator: ViewControllerDelegate {
    func go1Tapped() {
        let gotoVC = ViewController()
        gotoVC.delegate = self
        self.window.rootViewController = gotoVC
        self.window.makeKeyAndVisible()
    }
    
    func go2Tapped() {
        let gotoVC = ViewController2()
        gotoVC.delegate = self
        self.window.rootViewController = gotoVC
        self.window.makeKeyAndVisible()
    }
    
    func go3Tapped() {
        let gotoVC = ViewController3()
        gotoVC.delegate = self
        self.window.rootViewController = gotoVC
        self.window.makeKeyAndVisible()
    }
}
