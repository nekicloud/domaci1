//
//  ViewController3.swift
//  Domaci1
//
//  Created by Luka on 23/04/2019.
//  Copyright © 2019 luka. All rights reserved.
//


import UIKit

class ViewController3: UIViewController {
    
    weak var delegate: ViewControllerDelegate?

    @IBAction func transferButtonTapped(_ sender: UIButton) {
        delegate?.go1Tapped()
    }
    
    @IBAction func transferButtonTapped2(_ sender: UIButton) {
        delegate?.go2Tapped()
    }
}
